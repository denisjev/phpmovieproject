<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Mis películas</title>
</head>

<body>
    <div class="container-fluid">
        <?php
        if (isset($_POST['actualizarPelicula'])) {
            $id = $_POST['id'];
            $titulo = $_POST['titulo'];
            $descripcion = $_POST['descripcion'];
            $año = $_POST['anyo'];
            $categoria = $_POST['categoria'];
  
            $update_poster = "";

            if (is_uploaded_file($_FILES['poster']['tmp_name'])) {
                $nombreDirectorio = "poster/";
                $idUnico = time();
                $nombreFichero = $idUnico . "-" . $_FILES['poster']['name'];
                move_uploaded_file(
                    $_FILES['poster']['tmp_name'],
                    $nombreDirectorio . $nombreFichero
                );
                $poster = $nombreDirectorio . $nombreFichero;
                $update_poster = ", poster = '$poster'";
            }

            $db = new mysqli("localhost", "root", "", "bdcine");

            if ($db->connect_errno) {
                print "Error en la conexión " . $db->connect_errno;
                exit();
            }

            $stmt = "update peliculas set 
                        titulo = '$titulo', 
                        descripcion = '$descripcion', 
                        año = $año,
                        categoria = '$categoria'" . $update_poster . " where id = $id";
                        
            $resultado = $db->query($stmt);

            if ($resultado == true)
                $msg = "Dato actualizado correctamente";
            else
                $msg = "Error al actualizar los datos";
        } else
            $msg = "No se han recibido datos";
        ?>
        <div class="alert alert-primary" role="alert">
            <?php echo $msg; ?>
            <p><a href="index.php" class="btn btn-danger">Regresar</a></p>
        </div>
    </div>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>