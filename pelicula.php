<?php

class Pelicula {
    public $id;
    public $titulo;
    public $descripcion;
    public $año;
    public $categoria;
    public $poster;

    function __construct($i=0, $t="", $d="", $a=0, $c="", $p="")
    {
        $this->id=$i;
        $this->titulo=$t;
        $this->descripcion=$d;
        $this->año=$a;
        $this->categoria=$c;
        $this->poster=$p;
    }
}

?>