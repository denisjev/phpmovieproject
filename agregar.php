<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Mis películas</title>
</head>

<body>
    <div class="container-fluid">
        <h1>Nueva película</h1>
        <form method="POST" action="insertar.php" enctype="multipart/form-data" class="card p-2 col-md-3">
            <p>Título: <input type="text" name="titulo" class="form-control" /></p>
            <p>Descripción: <input type="text" name="descripcion" class="form-control" /></p>
            <p>Año: <input type="number" name="anyo" class="form-control" /></p>
            <p>Categoría: <select name="categoria" class="form-control">
                    <option value="Acción">
                        Acción
                    </option>
                    <option value="Aventura">
                        Aventura
                    </option>
                    <option value="Romance">
                        Romance
                    </option>
                    <option value="Suspenso">
                        Suspenso
                    </option>

                </select></p>
            <p>Poster: <input type="file" name="poster" class="form-control" /></p>
            <p><input type="submit" name="agregarPelicula" value="Guardar" class="btn btn-success" />
                <a href="index.php" class="btn btn-danger">Regresar</a>
            </p>
        </form>
    </div>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>