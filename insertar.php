<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Mis películas</title>
</head>

<body>
    <div class="container-fluid">

        <?php

        if (isset($_POST['agregarPelicula'])) {
            $titulo = $_POST['titulo'];
            $descripcion = $_POST['descripcion'];
            $año = $_POST['anyo'];
            $categoria = $_POST['categoria'];

            $nombre_poster = "";
            if (is_uploaded_file($_FILES['poster']['tmp_name'])) {
                $nombreDirectorio = "poster/";
                $idUnico = time();
                $nombreFichero = $idUnico . "-" . $_FILES['poster']['name'];
                move_uploaded_file(
                    $_FILES['poster']['tmp_name'],
                    $nombreDirectorio . $nombreFichero
                );
                $nombre_poster = $nombreDirectorio . $nombreFichero;
            } else
                print("No se ha podido subir el fichero\n");

            $db = new mysqli("localhost", "root", "", "bdcine");

            if ($db->connect_errno) {
                print "Error en la conexión " . $db->connect_errno;
                exit();
            }

            $stmt = "insert into peliculas(titulo, descripcion, año, categoria, poster) 
            value ('$titulo', '$descripcion', $año, '$categoria', '$nombre_poster')";

            $resultado = $db->query($stmt);

            if ($resultado == true)
                $msg = "Dato insertado correctamente";
            else
                $msg = "Error al insertar los datos";
        } else
            $msg = "No se han recibido datos";
        ?>
        <div class="alert alert-primary" role="alert">
            <?php echo $msg; ?>
            <p><a href="index.php" class="btn btn-danger">Regresar</a></p>
        </div>
    </div>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>