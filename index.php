<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Mis películas</title>
</head>

<body>
    <div class="container-fluid">
        <a href="agregar.php" class="btn btn-primary">Nueva película</a>
        <hr />

        <?php

        include_once('pelicula.php');

        $db = new mysqli("localhost", "root", "", "bdcine");

        if ($db->connect_errno) {
            print "Error en la conexión " . $db->connect_errno;
            exit();
        }

        $stmt = "select * from peliculas";

        $result = $db->query($stmt);

        $listaObjetosPeliculas = array();

        for ($i = 0; $i < mysqli_num_rows($result); $i++) 
        {
            $row = $result->fetch_assoc();

            $listaObjetosPeliculas[$i] = new Pelicula(
                $row['id'],
                $row['titulo'],
                $row['descripcion'],
                $row['año'],
                $row['categoria'],
                $row['poster']
            );
        }

        echo "<div class='row'>";
        foreach ($listaObjetosPeliculas as $pelicula) 
        {
        ?>
            <div class="card col-md-2 m-1">
                <img src="<?php echo $pelicula->poster; ?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $pelicula->titulo; ?></h5>
                    <small><?php echo $pelicula->año . " " . $pelicula->categoria; ?></small>
                    <p class="card-text"><?php echo $pelicula->descripcion; ?></p>

                    <a href="editar.php?id=<?php echo $pelicula->id; ?>" class="card-link">Editar</a>
                    <a href="borrar.php?id=<?php echo $pelicula->id; ?>" class="card-link">Eliminar</a>
                </div>
            </div>
        <?php
        }
        echo "</div>";
        ?>
    </div>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>