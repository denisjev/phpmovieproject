<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Mis películas</title>
</head>

<body>
    <div class="container-fluid">
        <?php
        if (isset($_POST['eliminarPelicula'])) {
            $id = $_POST['id'];

            $db = new mysqli("localhost", "root", "", "bdcine");

            if ($db->connect_errno) {
                print "Error en la conexión " . $db->connect_errno;
                exit();
            }

            $stmt = "delete from peliculas where id = $id";

            $resultado = $db->query($stmt);

            if ($resultado == true)
                $msg = "Dato eliminado correctamente";
            else
                $msg = "Error al eliminar los datos";
        } else
            $msg = "No se han recibido datos";
        ?>
        <div class="alert alert-primary" role="alert">
            <?php echo $msg; ?>
            <p><a href="index.php" class="btn btn-danger">Regresar</a></p>
        </div>
    </div>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>