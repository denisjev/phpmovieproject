<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Mis películas</title>
</head>

<body>
    <div class="container-fluid">
        <h1>Eliminar película</h1>

        <?php
        include_once("pelicula.php");

        if (isset($_GET['id'])) {
            $id = $_GET['id'];

            $db = new mysqli("localhost", "root", "", "bdcine");

            if ($db->connect_errno) {
                print "Error en la conexión " . $db->connect_errno;
                exit();
            }

            $stmt = "select * from peliculas where id = $id";

            $result = $db->query($stmt);

            $personaActual = new Pelicula();

            if (mysqli_num_rows($result) > 0) {
                $row = $result->fetch_assoc();
                $peliculaActual = new Pelicula(
                    $row['id'],
                    $row['titulo'],
                    $row['descripcion'],
                    $row['año'],
                    $row['categoria'],
                    $row['poster']
                );
            }

        ?>
            <form method="POST" action="eliminar.php" enctype="multipart/form-data" class="card p-2 col-md-3">
                <input type="hidden" name="id" value="<?php echo $peliculaActual->id; ?>" />
                <h3>Título: <?php echo $peliculaActual->titulo; ?></h3>
                <small><?php echo $peliculaActual->año . " - " . $peliculaActual->categoria; ?></small>
                <hr />
                <p class="mx-auto"><img width="150px" src="<?php echo $peliculaActual->poster; ?>" /></p>
                <small>Descripción: <?php echo $peliculaActual->descripcion; ?></small>
                
                
                <input type="submit" name="eliminarPelicula" value="Eliminar" class="btn btn-danger" />
            </form>
        <?php
        } else {
        ?>
            <div class="alert alert-primary" role="alert">
                <?php echo "No se han recibido datos"; ?>
                <p><a href="index.php" class="btn btn-danger">Regresar</a></p>
            </div>
        <?php
        }
        ?>
    </div>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>