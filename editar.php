<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title>Mis películas</title>
</head>

<body>
    <div class="container-fluid">
        <h1>Editar película</h1>

        <?php
        include_once("pelicula.php");

        if (isset($_GET['id'])) {
            $id = $_GET['id'];

            $db = new mysqli("localhost", "root", "", "bdcine");

            if ($db->connect_errno) {
                print "Error en la conexión " . $db->connect_errno;
                exit();
            }

            $stmt = "select * from peliculas where id = $id";

            $result = $db->query($stmt);

            $personaActual = new Pelicula();

            if (mysqli_num_rows($result) > 0) {
                $row = $result->fetch_assoc();
                $peliculaActual = new Pelicula(
                    $row['id'],
                    $row['titulo'],
                    $row['descripcion'],
                    $row['año'],
                    $row['categoria'],
                    $row['poster']
                );
            }

        ?>
            <form method="POST" action="actualizar.php" enctype="multipart/form-data" class="card p-2 col-md-3">
                <input type="hidden" name="id" value="<?php echo $peliculaActual->id; ?>" />
                <p>Título: <input type="text" name="titulo" class="form-control" value="<?php echo $peliculaActual->titulo; ?>" /></p>
                <p>Descripción: <input type="text" name="descripcion" class="form-control" value="<?php echo $peliculaActual->descripcion; ?>" /></p>
                <p>Año: <input type="number" name="anyo" class="form-control" value="<?php echo $peliculaActual->año; ?>" /></p>
                <p>Categoría: <select name="categoria" class="form-control">
                        <option value="Acción" <?php if ($peliculaActual->categoria == "Acción") echo "selected"; ?>>
                            Acción
                        </option>
                        <option value="Aventura" <?php if ($peliculaActual->categoria == "Aventura") echo "selected"; ?>>
                            Aventura
                        </option>
                        <option value="Romance" <?php if ($peliculaActual->categoria == "Romance") echo "selected"; ?>>
                            Romance
                        </option>
                        <option value="Suspenso" <?php if ($peliculaActual->categoria == "Suspenso") echo "selected"; ?>>
                            Suspenso
                        </option>

                    </select></p>
                <p><img width="60px" src="<?php echo $peliculaActual->poster; ?>" /></p>
                <p>Poster: <input type="file" name="poster" class="form-control" value="<?php echo $peliculaActual->poster; ?>" /></p>
                <input type="submit" name="actualizarPelicula" value="Guardar" class="btn btn-success" />
            </form>
        <?php
        } else {
        ?>
            <div class="alert alert-primary" role="alert">
                <?php echo "No se han recibido datos"; ?>
                <p><a href="index.php" class="btn btn-danger">Regresar</a></p>
            </div>
        <?php
        }
        ?>
    </div>
    <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>